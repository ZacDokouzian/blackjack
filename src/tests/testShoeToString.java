package tests;

import model.*;

public class testShoeToString {
	
	public static void main(String args[]){
	Shoe shoe = new Shoe(1);
	int i=0;
	Card temp = new Card(Rank.Ace,Suit.Spades);
	while(i<10){
		temp = shoe.getNextCard();
		System.out.printf("Card: %s of %s\n",temp.getRank().toString(),temp.getSuit().toString());
		i++;
		}
	System.out.printf("===============================================\n");
	shoe.toString(false);
	System.out.printf("================================================\n");
	shoe.toString(true);
	}
	
}
