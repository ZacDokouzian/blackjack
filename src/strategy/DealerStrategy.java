package strategy;

import model.Hand;

public class DealerStrategy {
	boolean hitS17;
	public DealerStrategy(boolean dealerHitsS17){
		hitS17 = dealerHitsS17;
	}
	
	public Move getMove(Hand hand){
		int total = hand.getTotal();
		if(hitS17){
			if(total >= 17)
				return Move.Stand; 
			return Move.Hit;
		}
		if(total < 17 || (total == 17 && hand.isSoft()))
			return Move.Hit;
		return Move.Stand;
		
	}
}
