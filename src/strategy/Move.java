package strategy;

public enum Move {
	Hit, Stand, Double, Split, Insure, Surrender, None;
}
