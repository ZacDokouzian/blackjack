
package view;

import java.util.ArrayList;
import java.util.Scanner;

import strategy.DealerStrategy;
import strategy.Move;
import model.Card;
import model.Hand;
import model.Player;
import model.Rank;
import model.Shoe;

public class textBasedDealerPlayer {
	public static void main(String args[]){
		ArrayList<Player> players = new ArrayList<Player>();
		Player player = new Player(100);
		players.add(player);
		Shoe shoe = new Shoe(4);
		DealerStrategy dealer = new DealerStrategy(true);
		gameLoop(players,shoe,dealer);
	}
	private static void gameLoop(ArrayList<Player> players,Shoe shoe, DealerStrategy dealer){
		System.out.println("in game loop");
		int numHands = players.size()+1;
		System.out.printf("num hands %d\n",numHands);
		System.out.printf("deck left %f\n",shoe.getDeckLeft());
		while(shoe.getDeckLeft() >= .2f){
			System.out.println("in while");
			Card cards[] = new Card[2*numHands];
			for(int i=0;i<2*numHands;i++){
				cards[i] = shoe.getNextCard();
			}
			for(int i=0;i<players.size();i++){
				Hand hand = new Hand(cards[i],cards[i+numHands]);
				players.get(i).addHand(5, hand);
			}
			Hand dealerHand = new Hand(cards[players.size()],cards[2*numHands-1]);
			System.out.println("leaving game loop");
			playHand(players,shoe,dealer,dealerHand);
		}
	}
	private static void playHand(ArrayList<Player> players, Shoe shoe, DealerStrategy dealer,Hand dealerHand){
		for(Player player:players){
			printInitInfo(player);
		}
		Scanner scan = new Scanner(System.in);
		ArrayList<Move> moves = new ArrayList<Move>();
		Move theMove;
		moves.add(Move.Double);
		moves.add(Move.Hit);
		moves.add(Move.Stand);
		moves.add(Move.Surrender);
		System.out.println();
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		System.out.printf("Dealer Holds\t%s, ????????\n",dealerHand.getCard(0).toString());
		if(dealerHand.getCard(0).getRank() == Rank.Ace){
			moves.add(Move.Insure);
		}
		for(int i=0;i<players.size();i++){
			System.out.printf("Player %d Holds\t%s, %s\tFor a total of %d\n",i+1,players.get(i).getHand().getCard(0).toString(),players.get(i).getHand().getCard(1).toString(),players.get(i).getHand().getTotal());
			if(players.get(i).getHand().getCard(0).getRank().compareTo(players.get(i).getHand().getCard(1).getRank()) == 0){
				moves.add(Move.Split);
			}
			printMoves(moves);
			String textEntered;
			System.out.println("You choose to:");
			textEntered = scan.nextLine();
			theMove = getMove(textEntered,moves);
			while(theMove != Move.Stand || theMove != Move.None){
				
			}
		}
	}
	
	
	private static Move getMove(String str,ArrayList<Move> moves){
		Move toReturn = Move.None;		
		if(str.compareToIgnoreCase("hit") == 0 || str.compareToIgnoreCase("h") == 0){
			if(moves.contains(Move.Hit)){
				toReturn = Move.Hit;
			}
		}
		else if(str.compareToIgnoreCase("stand") == 0 || str.compareToIgnoreCase("s") == 0){
			if(moves.contains(Move.Stand)){
				toReturn = Move.Stand;
			}
		}
		else if(str.compareToIgnoreCase("double") == 0 || str.compareToIgnoreCase("d") == 0){
			if(moves.contains(Move.Double)){
				toReturn = Move.Double;
			}
		}
		else if(str.compareToIgnoreCase("surrender") == 0){
			if(moves.contains(Move.Surrender)){
				toReturn = Move.Surrender;
			}
		}
		else if(str.compareToIgnoreCase("insure") == 0 || str.compareToIgnoreCase("i") == 0){
			if(moves.contains(Move.Insure)){
				toReturn = Move.Insure;
			}
		}
		else if(str.compareToIgnoreCase("split") == 0){
			if(moves.contains(Move.Split)){
				toReturn = Move.Split;
			}
		}
		
		return toReturn;
	}
	private static void printMoves(ArrayList<Move> moves){
		System.out.printf("available moves:\n");
		for(int i =0;i<moves.size();i++){
			System.out.printf("%s\t",moves.get(i).toString());
		}
		System.out.printf("\n");
	}
	private static void printInitInfo(Player player){
		System.out.printf("Total cash: %d\n", player.getMoney());
		System.out.printf("Net session profit: %d\n",player.getMoney()-player.getInitialMoney());
	}
}
