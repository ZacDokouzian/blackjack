package view;

import strategy.DealerStrategy;
import strategy.Move;
import model.Hand;
import model.Shoe;

//Deal 10 hands to dealer AI see if it responds correctly
public class TextBasedDealerOnly {
	public static void main(String args[]){
		Shoe shoe = new Shoe(4);
		DealerStrategy dealer = new DealerStrategy(true);
		int i=0;
		while(i<10){
			Hand dealerHand = new Hand(shoe.getNextCard(),shoe.getNextCard());
			System.out.printf("~~~~~~New Hand~~~~~~~.\nDealer: %s of %s, %s of %s\n",
					dealerHand.getCard(0).getRank().toString(),dealerHand.getCard(0).getSuit().toString(),
					dealerHand.getCard(1).getRank().toString(),dealerHand.getCard(1).getSuit().toString());
			Move nextMove = dealer.getMove(dealerHand);
			boolean bust = false;
			System.out.printf("Dealer chooses to %s\n",nextMove.toString());
			while(nextMove != Move.Stand && !bust){
				dealerHand.addCard(shoe.getNextCard());
				for(int j=0;j<dealerHand.getTotalCards();j++){
					System.out.printf("%s of %s, ", dealerHand.getCard(j).getRank().toString(),dealerHand.getCard(j).getSuit().toString());
				}
				System.out.println();
				nextMove = dealer.getMove(dealerHand);
				if(dealerHand.getTotal() > 21){
					bust = true;
					System.out.println("BUSTED!!!");
				}
				else{
					System.out.printf("Dealer chooses to %s with %d\n",nextMove.toString(),dealerHand.getTotal());
				}
			}
			System.out.printf("~~~~~~~~~~~~~~~~~~~~~~~End Hand~~~~~~~~~~~~~~~~~~~~~\n");
			i++;
		}
	}
}
