package model;

public class Card {
	Rank rank;
	Suit suit;
	public Card(Rank theRank,Suit theSuit){
		rank = theRank;
		suit = theSuit;
	}
	public Rank getRank(){
		return this.rank;
	}
	public Suit getSuit(){
		return this.suit;
	}
	public String toString(){
		String str = "";
		str += this.rank.toString();
		str += " of ";
		str += this.suit.toString();
		return str;
	}
}
