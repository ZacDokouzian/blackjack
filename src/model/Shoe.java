package model;

import java.util.Random;


public class Shoe {
	int decks;
	int cardsLeft;
	int currentCard = 0;
	int totalCards;
	Card theShoe[];
	//initialize and shuffle a shoe containing numDecks decks of cards
	public Shoe(int numDecks){
		totalCards = 52*numDecks;
		cardsLeft = totalCards;
		theShoe = new Card[totalCards];
		//create a placeholder card, rank and suit will be determined below
		Card newCard = new Card(Rank.Ace,Suit.Spades);
		for(int i=0;i<numDecks;i++){
			for(int j=0;j<4;j++){
				for(int k=0;k<13;k++){
					if(j==0){
						newCard.suit = Suit.Diamonds;
					}
					else if(j==1){
						newCard.suit = Suit.Clubs;
					}
					else if(j==2){
						newCard.suit = Suit.Hearts;
					}
					else{
						newCard.suit = Suit.Spades;
					}
					
					if(k == 0){
						newCard.rank = Rank.Ace;
					}
					else if(k==1){
						newCard.rank = Rank.Deuce;
					}
					else if(k==2){
						newCard.rank = Rank.Three;
					}
					else if(k==3){
						newCard.rank = Rank.Four;
					}
					else if(k==4){
						newCard.rank = Rank.Five;
					}
					else if(k==5){
						newCard.rank = Rank.Six;
					}
					else if(k==6){
						newCard.rank = Rank.Seven;
					}
					else if(k==7){
						newCard.rank = Rank.Eight;
					}
					else if(k==8){
						newCard.rank = Rank.Nine;
					}
					else if(k==9){
						newCard.rank = Rank.Ten;
					}
					else if(k==10){
						newCard.rank = Rank.Jack;
					}
					else if(k==11){
						newCard.rank = Rank.Queen;
					}
					else{
						newCard.rank = Rank.King;
					}
					theShoe[52*i+13*j+k] = new Card(newCard.rank,newCard.suit);
				}
			}
		}
		shuffle();
	}
	
	//fisher-yates shuffle algorithm
	private void shuffle(){
		Random rand = new Random(System.currentTimeMillis());
		int j;
		for(int i = totalCards-1;i>0;i--){
			j = rand.nextInt(i);
			swapCards(i,j);
		}
	}
	
	//swaps cards at indexes a and b in the shoe
	private void swapCards(int a,int b){
		Card temp = new Card(theShoe[a].rank,theShoe[a].suit);
		theShoe[a] = theShoe[b];
		theShoe[b] = temp;
		return;
	}
	
	public Card getNextCard(){
		Card nextCard = theShoe[currentCard];
		currentCard++;
		cardsLeft--;
		return nextCard; 
	}
	
	public void toString(boolean fullShoe){
		if(fullShoe){
			for(int i=0;i<totalCards;i++){
				System.out.printf("Card%d:\t%s of %s\n",i,theShoe[i].rank.name(),theShoe[i].suit.name() );
			}
			return;
		}
		int j = currentCard;
		while(j<totalCards){
			System.out.printf("Card%d:\t%s of %s\n",j,theShoe[j].rank.name(),theShoe[j].suit.name());
			j++;
		}
		return;
	}
	//return the portion of the deck remaining. Half shoe returns .5, quarter shoe .25 ect...
	public float getDeckLeft (){
		return (float)(totalCards-currentCard)/(float)totalCards;
	}
}
