package model;

public enum Suit {
Diamonds(1), Clubs(2), Hearts(3), Spades(4);
	
	private int num;
	Suit(int value){
		this.num = value;
	}
	
	public int getValue(){
		return num;
	}
}
