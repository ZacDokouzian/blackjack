package model;

import java.util.LinkedList;

public class Hand {
	int total;//the total the player would see in game, IE K3A = 14 AA = 12 A3A = 15
	int numCards;
	int aces=0;
	boolean soft = false;
	boolean blackJack = false;
	int timesSplit = 0;
	
	LinkedList<Card> hand;
	
	public Hand(Card first,Card second){
		hand = new LinkedList<Card>();
		hand.add(first);
		hand.add(second);
		numCards = 2;
		calculateTotal();
	}
	
	public boolean isSoft(){
		return soft;
	}
	public int getTotal(){
		return total;
	}
	public Card getCard(int index){
		return hand.get(index);
	}
	public int getTotalCards(){
		return numCards;
	}
	public int addCard(Card toAdd){
		hand.add(toAdd);
		numCards++;
		calculateTotal();
		return total;
	}
	
	private void calculateTotal(){
		total = 0;
		aces =0;
		for(int i=0;i<numCards;i++){
			total  += hand.get(i).getRank().getValue();
			if(hand.get(i).getRank().getValue() == 11)
				aces++;
		}
		int i=0;
		while(total > 21 && i<numCards){
			if(hand.get(i).getRank().getValue() == 11){
				total -= 10;
				aces--;
			}
			i++;
		}
		if(aces > 0)
			soft = true;
		else
			soft = false;
	}
}
