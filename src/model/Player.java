package model;

import java.util.LinkedList;
import strategy.PlayerStrategy;

public class Player {
	long buyInAmt;
	long currentMoney;
	int currentHand;
	LinkedList<Hand> hands;
	LinkedList<Long> wagers;
	PlayerStrategy strategy;
	
	public Player(long buyIn){
		buyInAmt = buyIn;
		currentMoney = buyIn;
		hands = new LinkedList<Hand>();
		wagers = new LinkedList<Long>();
	}
	
	public void addHand(long wagerAmt,Hand hand){
		currentMoney -= wagerAmt;
		wagers.add(wagerAmt);
		hands.add(hand);
	}
	public long getMoney(){
		return currentMoney;
	}
	
	public void payout(long wager){
		currentMoney += wager;
	}
	
	public long getInitialMoney(){
		return buyInAmt;
	}
	public Hand getHand(){
		return hands.getFirst();
	}
}
