package model;

public class HouseRules {
	public boolean hitS17;
	public int splits;
	public boolean resplitAces;
	public int decks;
	public boolean doubleAnyTwoCards;
	public boolean canSurrender;
	public int tableMin;
	public int tableMax;
	
	public HouseRules(boolean dealerHitsS17,int maxSplits,boolean RSA,int numDecks,boolean doubleATC,boolean surrender,int min,int max){
		hitS17 = dealerHitsS17;
		splits = maxSplits;
		resplitAces = RSA;
		decks = numDecks;
		doubleAnyTwoCards = doubleATC;
		canSurrender = surrender;
		tableMin = min;
		tableMax = max;
	}
}
